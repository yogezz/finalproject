package com.TaskManagement.jwt.dto;

import java.util.Date;

import com.TaskManagement.jwt.entity.User;

public class TaskDto {

	private Long taskId;
	private String taskDescription;
	private int taskNumber;
	private String assign;
	private Date startingDate;
	
	private Date estimateDate;
	
	private String taskStatus;
	
	private int sprint;

	public TaskDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaskDto(Long taskId, String taskDescription, int taskNumber, String assign, Date startingDate,
			Date estimateDate, String taskStatus, int sprint) {
		super();
		this.taskId = taskId;
		this.taskDescription = taskDescription;
		this.taskNumber = taskNumber;
		this.assign = assign;
		this.startingDate = startingDate;
		this.estimateDate = estimateDate;
		this.taskStatus = taskStatus;
		this.sprint = sprint;
	}


	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public int getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getAssign() {
		return assign;
	}

	public void setAssign(String assign) {
		this.assign = assign;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getEstimateDate() {
		return estimateDate;
	}

	public void setEstimateDate(Date estimateDate) {
		this.estimateDate = estimateDate;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public int getSprint() {
		return sprint;
	}

	public void setSprint(int sprint) {
		this.sprint = sprint;
	}

	
	
}
