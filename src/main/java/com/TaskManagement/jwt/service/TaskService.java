package com.TaskManagement.jwt.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TaskManagement.jwt.dao.TaskDao;
import com.TaskManagement.jwt.dao.UserDao;
import com.TaskManagement.jwt.dto.TaskDto;
import com.TaskManagement.jwt.dto.UserDto;
import com.TaskManagement.jwt.entity.Role;
import com.TaskManagement.jwt.entity.Task;
import com.TaskManagement.jwt.entity.User;


@Service
public class TaskService {
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private UserDao userDao;
	
	

	public TaskDto createTask(TaskDto taskDto) {
		// TODO Auto-generated method stub
		String userName= taskDto.getAssign();
		User user=userDao.findByUserName(userName);
		Task task=new Task();
		task.setTaskId(taskDto.getTaskId());
		task.setTaskDescription(taskDto.getTaskDescription());
		task.setTaskNumber(taskDto.getTaskNumber());
		task.setStartingDate(taskDto.getStartingDate());
		task.setEstimateDate(taskDto.getEstimateDate());
		task.setTaskStatus(taskDto.getTaskStatus());
		task.setSprint(taskDto.getSprint());
		task.setAssign(user);

		Task savedTask=taskDao.save(task);
		TaskDto taskDto1=new TaskDto();
		taskDto1.setTaskId(task.getTaskId());
		taskDto1.setTaskDescription(task.getTaskDescription());
		taskDto1.setTaskNumber(task.getTaskNumber());
		taskDto1.setStartingDate(task.getStartingDate());
		taskDto1.setEstimateDate(task.getEstimateDate());
		taskDto1.setTaskStatus(task.getTaskStatus());
		taskDto1.setSprint(task.getSprint());
		taskDto1.setAssign(task.getAssign().getUserName());
		return taskDto1;
	}

	

	public Task find(long taskId) {
		// TODO Auto-generated method stub
		return taskDao.findById(taskId).orElse(null);
	}


	



	public List<TaskDto> getAllTask() {
		// TODO Auto-generated method stub
		TaskDto task1=new TaskDto();
		List<Task> taskList= taskDao.findAll();
		List<TaskDto> dtoList=new ArrayList<>();
		
		for (Task data : taskList) {
			TaskDto dto = new TaskDto();
			dto.setTaskId(data.getTaskId());
			dto.setTaskNumber(data.getTaskNumber());
			dto.setTaskDescription(data.getTaskDescription());
			dto.setStartingDate(data.getStartingDate());
			dto.setEstimateDate(data.getEstimateDate());
			dto.setTaskStatus(data.getTaskStatus());
			dto.setSprint(data.getSprint());
			dto.setAssign(data.getAssign().getUserName());
			
			dtoList.add(dto);
			
			}
		return dtoList;
		
	}



	public void deleteTask(Long id) {
		// TODO Auto-generated method stub
		taskDao.deleteById(id);
		
	}

	
	public TaskDto updateTask(Long id, TaskDto task) {
		// TODO Auto-generated method stub
		Task tasks = taskDao.findById(id).orElseThrow();
		tasks.setTaskDescription(task.getTaskDescription());
		tasks.setTaskNumber(task.getTaskNumber());
		
		String userName= task.getAssign();
		User user=userDao.findByUserName(userName);
		
		tasks.setAssign(user);
		tasks.setStartingDate(task.getStartingDate());
		tasks.setEstimateDate(task.getEstimateDate());
		tasks.setTaskStatus(task.getTaskStatus());
		tasks.setSprint(task.getSprint());
		taskDao.save(tasks);
		
		task.setAssign(tasks.getAssign().getUserName());
		task.setTaskDescription(tasks.getTaskDescription());
		task.setTaskNumber(tasks.getTaskNumber());
		task.setStartingDate(tasks.getStartingDate());
		task.setEstimateDate(tasks.getEstimateDate());
		task.setTaskStatus(tasks.getTaskStatus());
		task.setSprint(tasks.getSprint());
		return task;
	}
	
	
	
	
	

}
