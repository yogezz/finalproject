package com.TaskManagement.jwt.service;

import com.TaskManagement.jwt.dao.RoleDao;
import com.TaskManagement.jwt.dao.UserDao;
import com.TaskManagement.jwt.dto.UserDto;
import com.TaskManagement.jwt.entity.Role;
import com.TaskManagement.jwt.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    
  

 

    public UserDto registerNewUser(UserDto userDto) {
       // Role role = roleDao.findAllById("User").get();
    	
       Role role=new Role();
       role.setRoleId(userDto.getRole());
        Set<Role> roleSet=new HashSet<>();
        roleSet.add(role);
        userDto.setUserPassword(getEncodedPassword(userDto.getUserPassword()));

        User user = mapToUser(userDto);
        user.setRole(roleSet);
		User savedUser = userDao.save(user);
		
		return mapToUserDto(savedUser);
    }

    public String getEncodedPassword(String password) {
        return passwordEncoder.encode(password);
    }
    
	public  UserDto mapToUserDto(User user) {
		return new UserDto(
				user.getUserId(),
				user.getUserName(),
				user.getUserFirstName(),
				user.getUserLastName(),
				user.getUserPassword(),
				user.getRole()
				);
	}
	
	public  User mapToUser(UserDto userDto) {
		return new User(
				userDto.getUserId(),
				userDto.getUserName(),
				userDto.getUserFirstName(),
				userDto.getUserLastName(),
				userDto.getUserPassword()
				
				);
	}

	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	
}
