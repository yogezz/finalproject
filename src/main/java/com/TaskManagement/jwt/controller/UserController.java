package com.TaskManagement.jwt.controller;

import com.TaskManagement.jwt.dao.UserDao;
import com.TaskManagement.jwt.dto.UserDto;
import com.TaskManagement.jwt.entity.User;
import com.TaskManagement.jwt.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.annotation.PostConstruct;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class UserController {

    @Autowired
    private UserService userService;

   

   
    @PostMapping({"/registerNewUser"})
    public UserDto registerNewUser(@RequestBody UserDto userDto) {
        return userService.registerNewUser(userDto);
    }

    
    @GetMapping("/getAllUser")
   public List<User>   getAllUser() {
	   return userService.getAllUser();
   }
    
    
}
