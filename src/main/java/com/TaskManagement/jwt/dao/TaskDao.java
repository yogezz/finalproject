package com.TaskManagement.jwt.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.TaskManagement.jwt.entity.Task;

public interface TaskDao extends JpaRepository<Task, Long> {
	
}
